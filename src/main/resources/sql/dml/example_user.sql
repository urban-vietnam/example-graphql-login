-- 綺麗さっぱりにしてからDML実行
TRUNCATE TABLE m_users CASCADE;
INSERT INTO m_users VALUES
 ('0000000001', 'yhiguchi', 'yhiguchi@urban-web.co.jp', '$2a$10$ocV1OLnieLWbhXHM6vlbeewRopzPQKKfX3w23uuTV.pb9R3IDDYOe')
 , ('0000000002', 'ssata', 'ssata@urban-web.co.jp', '$2a$10$ocV1OLnieLWbhXHM6vlbeewRopzPQKKfX3w23uuTV.pb9R3IDDYOe')
;

TRUNCATE TABLE m_user_profiles CASCADE;
INSERT INTO m_user_profiles VALUES
 ('0000000001', 'higuchi', 'yosuke')
 , ('0000000002', 'sata', 'shinpei')
;

TRUNCATE TABLE m_user_roles CASCADE;
INSERT INTO m_user_roles VALUES
 ('0000000001', 'USER'), ('0000000001', 'ADMIN') -- yhiugu has admin
 , ('0000000002', 'USER') -- ssata has not admin
;
-- シーケンスも綺麗に
ALTER SEQUENCE user_id_sequence RESTART WITH 3;