
-- まずは削除
DROP TABLE IF EXISTS m_user_roles;
DROP TABLE IF EXISTS m_user_profiles;
DROP TABLE IF EXISTS m_users;
DROP SEQUENCE IF EXISTS user_id_sequence;

-- シーケンス ユーザーID管理シーケンス
CREATE SEQUENCE user_id_sequence
  INCREMENT BY 1
  MAXVALUE 9999999999
  START WITH 1
  NO CYCLE
;
-- ユーザー 管理テーブル
CREATE TABLE "m_users" (
  "id" varchar PRIMARY KEY DEFAULT to_char(nextval('user_id_sequence'),'FM0000000000'),
  "nickname" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "password" varchar NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp NOT NULL DEFAULT (CURRENT_TIMESTAMP)
);

-- ユーザープロフィール 管理テーブル
CREATE TABLE "m_user_profiles" (
  "id" varchar PRIMARY KEY,
  "family_name" varchar,
  "last_name" varchar,
  "created_at" timestamp NOT NULL DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp NOT NULL DEFAULT (CURRENT_TIMESTAMP)
);

ALTER TABLE "m_user_profiles" ADD FOREIGN KEY ("id") REFERENCES "m_users" ("id");

-- ユーザー権限 管理テーブル
CREATE TABLE "m_user_roles" (
  "id" varchar,
  "role_type" varchar,
  PRIMARY KEY ("id", "role_type")
);

ALTER TABLE "m_user_roles" ADD FOREIGN KEY ("id") REFERENCES "m_users" ("id");