package vn.urban.vietnam.demologin.types;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.urban.vietnam.demologin.model.value.UserId;
import vn.urban.vietnam.demologin.model.value.UserRoleType;

import java.io.Serializable;
import java.util.Set;

/**
 * ユーザー 情報
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** ユーザー基本 情報 */
	private UserId id;
	/** ユーザー権限 情報 一覧 */
	@Setter
	private Set<UserRoleType> roleTypes;
	/** ユーザープロフィール情報 */
	@Setter
	private Profile profile;
}
