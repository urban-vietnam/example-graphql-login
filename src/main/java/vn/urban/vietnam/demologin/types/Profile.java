package vn.urban.vietnam.demologin.types;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.urban.vietnam.demologin.model.value.UpdatedAt;
import vn.urban.vietnam.demologin.model.value.UserId;

import java.io.Serializable;

/**
 * ユーザー プロフィール情報
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Profile implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	private UserId id;
	@Setter
	private String nickname;
	@Setter
	private String email;
	@Setter
	private String familyName;
	@Setter
	private String lastName;
	private UpdatedAt updateAt;
}
