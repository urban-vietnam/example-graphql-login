package vn.urban.vietnam.demologin.types;

/**
 * ユーザーのプロフィール 更新値
 */
public interface SaveProfileInput {
	public String getFamilyName();
	public String getLastName();
}
