package vn.urban.vietnam.demologin.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import vn.urban.vietnam.demologin.model.PreAuthenticatedAuthenticationTokenFactory;
import vn.urban.vietnam.demologin.service.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class MyJWTFilter extends OncePerRequestFilter {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	/** JWT の埋め込みされている HttpHeader Key名 */
	private static final String AUTHORIZATION_HEADER = "Authorization";
	/** JWT の埋め込みされている HttpHeader Valueの JWT以外の値 */
	private static final Pattern BEARER_PATTERN = Pattern.compile("^Bearer (.+?)$");
	/*********************************************
	 * Filed
	 *********************************************/
	/** User サービス */
	private final UserService userService;
	/** JWT事前認証結果 生成器 */
	private final PreAuthenticatedAuthenticationTokenFactory preAuthenticationTokenFactory;

	/*********************************************
	 * 独自の実装追加 (OncePerRequestFilter)
	 *********************************************/
	/**
	 * doFilter (認証していいものかの事前確認)のインターセプト
	 *
	 * Same contract as for {@code doFilter}, but guaranteed to be
	 * just invoked once per request within a single request thread.
	 * See {@link #shouldNotFilterAsyncDispatch()} for details.
	 * <p>Provides HttpServletRequest and HttpServletResponse arguments instead of the
	 * default ServletRequest and ServletResponse ones.
	 *
	 * @param request Http Request リクエスト内容
	 * @param response Http Response レスポンス内容
	 * @param filterChain 認証フィルターチェーン = JWT認証フィルター の内容を追加する先
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		// HttpRequest HeaderのJWTの値から JWTの値にて認証を実施し
		toToken(request) // HttpRequest HeaderのJWTの値から
			.map(this.userService::loadUserByToken) // JWTの値にて認証を実施し
			.map((userDetails) -> this.preAuthenticationTokenFactory.create(userDetails, request)) // UserDetails -> PreAuthenticatedAuthenticationTokenへ (認可チェックできる形へ)
			.ifPresent(authentication -> SecurityContextHolder.getContext().setAuthentication(authentication)) // 認証できるJWTであったら 認証結果を保存
		;
		// このJWT通信認証以外を 実施
		filterChain.doFilter(request, response);
	}

	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * Http Request -> JWT string
	 * @param request Http Request
	 * @return JWTの文字列
	 */
	private Optional<String> toToken(HttpServletRequest request) {
		// HttpRequestの JWT格納HttpHeader値から JWT格納HttpHeaderからJWT以外の値を 取り除いたJWTに該当する文字列 を抽出
		return Optional
				.ofNullable(request.getHeader(AUTHORIZATION_HEADER))// HttpRequestの JWT格納HttpHeader値から
				.filter((v) -> !Objects.isNull(v) && !v.isEmpty()) // 空文字列 以外のもので
				.map(BEARER_PATTERN::matcher) // JWT格納HttpHeaderからJWT以外の値を
				.filter(Matcher::find)
				.map(matcher -> matcher.group(1)); // 取り除いたJWTに該当する文字列 を抽出
	}
}
