package vn.urban.vietnam.demologin.config.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.time.Duration;

// TODO:security.propertiesなどの外部ファイルから設定 一旦適当に
@ConstructorBinding
@ConfigurationProperties(prefix = "security")
@Getter
@RequiredArgsConstructor
public class SecurityProperties {
	/**
	 * パスワードのハッシュ 暗号化の時の 暗号化する回数(ストレッチ回数) (指定回数の2乗)
	 * Amound of hashing iterations, where formula is 2^passwordStrength iterations
	 */
	private final int passwordStrength = 10; // 10回に特に意味はない 確かデフォルト
	/**
	 * JWT暗号化文字列を生成するときの HMAC SHA-256の 秘密鍵
	 * TODO:特に意味はない
	 * Secret key used to generate and verify JWT tokens
	 */
	private final String tokenSecret = "tnakahama";
	/**
	 * JWT発行時の発行者名
	 * Name of the my JWT issuer
	 */
	private final String tokenIssuer = "urban-vietnam-sample";
	/**
	 * JWTの有効期限
	 * Duration after which a JWT will expire
	 * TODO: 取り敢えず 4時間
	 * TODO: 外部ファイルで設定?
	 */
	private final Duration tokenExpiration = Duration.ofHours(4); // 4 時間の意味は特にない
}
