package vn.urban.vietnam.demologin.config.security;

import lombok.Builder;
import lombok.Getter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.io.Serializable;

/**
 * MyUserDetailsのユーザー情報から得た権限値を保管し
 * SpringSecurityの @PreAuthorize("hasAuthority('ADMIN')") チェックなどの PreAuthorizeとして参照させるための値
 */
@Getter
public class MyPreAuthenticationToken extends PreAuthenticatedAuthenticationToken implements Serializable {
	/*********************************************
	 * constructor
	 *********************************************/
	@Builder
	public MyPreAuthenticationToken(UserDetails principal, WebAuthenticationDetails details) {
		super(principal, null, principal.getAuthorities());
		super.setDetails(details);
	}

	/*********************************************
	 * override
	 *********************************************/
	/**
	 * @return 外部API参照などによる APIキー認証 のやつは今回利用しない
	 */
	@Override
	public Object getCredentials() { return null; }
}
