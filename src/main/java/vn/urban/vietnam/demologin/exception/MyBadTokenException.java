package vn.urban.vietnam.demologin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 認証JWT の値が不正 エラー
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class MyBadTokenException extends RuntimeException{
	/**
	 * @return 独自のメッセージ出力
	 */
	@Override
	public String getMessage() { return "Token is invalid or expired"; } // トークンが不正または有効期限切れ
}

