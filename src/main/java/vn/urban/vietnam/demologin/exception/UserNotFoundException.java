package vn.urban.vietnam.demologin.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import vn.urban.vietnam.demologin.model.value.UserId;

import java.text.MessageFormat;

/**
 * domain exception class (ユーザー存在しない例外)
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
@RequiredArgsConstructor
public class UserNotFoundException extends RuntimeException {
	/** find user id */
	private final UserId id;

	/**
	 * @return 独自のメッセージ出力
	 */
	@Override
	public String getMessage() {
		return MessageFormat.format("User with ID ''{0}'' isn''t available", id.toString());
	}
}
