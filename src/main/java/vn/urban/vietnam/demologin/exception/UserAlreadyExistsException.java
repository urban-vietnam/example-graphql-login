package vn.urban.vietnam.demologin.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

/**
 * domain exception class (ユーザー存在しない例外)
 */
@ResponseStatus(HttpStatus.CONFLICT)
@RequiredArgsConstructor
public class UserAlreadyExistsException extends RuntimeException {
	/** duplicate Email */
	private final String email;

	@Override
	public String getMessage() {
		return MessageFormat.format("A user already exists with email ''{0}''", email);
	}
}
