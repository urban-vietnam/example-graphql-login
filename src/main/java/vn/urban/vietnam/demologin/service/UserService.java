package vn.urban.vietnam.demologin.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import vn.urban.vietnam.demologin.exception.MyBadTokenException;
import vn.urban.vietnam.demologin.exception.UserAlreadyExistsException;
import vn.urban.vietnam.demologin.infrastructure.MUserRepository;
import vn.urban.vietnam.demologin.infrastructure.entity.MUser;
import vn.urban.vietnam.demologin.infrastructure.entity.MUserProfile;
import vn.urban.vietnam.demologin.model.UserDetailsFactory;
import vn.urban.vietnam.demologin.model.UserFactory;
import vn.urban.vietnam.demologin.model.value.CreatedAt;
import vn.urban.vietnam.demologin.model.value.UpdatedAt;
import vn.urban.vietnam.demologin.model.value.UserId;
import vn.urban.vietnam.demologin.config.security.SecurityProperties;
import vn.urban.vietnam.demologin.model.value.UserRoleType;
import vn.urban.vietnam.demologin.types.CreateUserInput;
import vn.urban.vietnam.demologin.types.User;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
	/*********************************************
	 * filed
	 *********************************************/
	/** 対象のリポジトリ */
	private final MUserRepository repository;
	/** 型 生成器 */
	private final UserDetailsFactory userDetailsFactory;
	private final UserFactory userFactory;
	/** 環境設定値 */
	private final SecurityProperties properties; // このプロジェクトのセキュリティ設定値
	private final Algorithm algorithm; // JWT 生成 アルゴリズム
	private final JWTVerifier verifier; // JWT 解析者
	private final PasswordEncoder passwordEncoder; // パスワード暗号器

	/*********************************************
	 * 外部参照可能関数 (UserDetailsServiceとしての実装)
	 *********************************************/
	/**
	 * login ID から 認可ユーザー情報 を読み込み
	 * @param email login id = emailとしているので emailの文字列
	 * @return login id に該当する 認証ユーザー情報
	 * @throws UsernameNotFoundException 指定ユーザーがいないエラー if the user could not be found or he user has no GrantedAuthority
	 */
	@Override
	@Transactional // 認証は認証の間でのトランザクション管理設定にする
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// 前提条件
		if (Objects.isNull(email) || email.isEmpty()) throw new UsernameNotFoundException(""); // ログインIDの指定なし

		// 指定 email(loginId)に該当する ユーザー情報を検索し 認証ユーザー情報 へ変換する
		return repository.findByEmail(email) // 指定 loginIdに該当する ユーザー情報を検索
				.map(this.userDetailsFactory::create) // 検索結果 を login ID に該当する 認証ユーザー情報へ
				.orElseThrow(() -> new UsernameNotFoundException("メールアドレスまたはパスワードに誤りがあります。")); // 検索結果 見つからない場合はエラー
	}
	/**
	 * JWT から 認可ユーザー情報 を読み込み (JWTログイン)
	 * @param token 対象となるJWT文字列
	 * @return JWT に該当する 認可ユーザー情報
	 * @throws UsernameNotFoundException 指定ユーザーがいないエラー if the user could not be found or the user has no GrantedAuthority
	 */
	@Transactional // 認証は認証の間でのトランザクション管理設定にする
	public UserDetails loadUserByToken(String token) throws UsernameNotFoundException {
		// 前提条件
		if (Objects.isNull(token) || token.isEmpty()) throw new MyBadTokenException(); // JWTの指定なし

		// 指定 JWTを複合化し 付け加えた情報(login ID)を解析し 解析したユーザー識別子からユーザー情報を検索し 認可ユーザー情報 へ変換する
		return decodedToken(token) // 指定 JWTを複合化
				.map(DecodedJWT::getSubject) // ユーザー識別子を解析し
				.map(UserId::of) // ユーザー識別子の型へ変換
				.flatMap(repository::findById) // ユーザー識別子 に該当するユーザー情報を検索し
				.map(this.userDetailsFactory::create) //認可ユーザー情報 へ変換する
				.orElseThrow(MyBadTokenException::new); // 見つからない場合などは JWTの値不正エラー とする
	}
	/*********************************************
	 * 外部参照可能関数 (UserDetailsServiceとしての権限確認実装)
	 *********************************************/
	/**
	 * 認証済みの状態確認(通信確認)
	 * @return true:認証済み / false: それ以外
	 */
	public boolean isAuthenticated() {
		return Optional
				.ofNullable(SecurityContextHolder.getContext()) // 認証情報をコンテキストから取得
				.map(SecurityContext::getAuthentication)
				.filter(Authentication::isAuthenticated) // 認証状態 = 認証済みであること (基本確認)
				.filter((authenticated) -> !(this.isAnonymous(authenticated))) // 認証状態 = 未認証の状態ではない こと(追加確認)
				.isPresent(); // 認証済み -> 結果存在
	}

	/**
	 * 未認証の状態である
	 * @param authentication 確認する認証の状態
	 * @return true:未認証(認証を未だしていない) /false:それ以外
	 */
	private boolean isAnonymous(Authentication authentication) {
		return authentication instanceof AnonymousAuthenticationToken;
	}

	/*********************************************
	 * 外部参照可能関数 (Domain Service)
	 *********************************************/
	/**
	 * @return 認証されたコンテキストの情報ぁら 該当するユーザー情報を取得
	 */
	@Transactional
	public User getCurrentUser() {
		// 認証されたユーザー情報から ユーザ識別子を取得し ユーザー識別子にて検索したユーザー情報(Entity)を User typeにして返却
		return Optional
				.ofNullable(SecurityContextHolder.getContext()) // 認証されたユーザー情報から
				.map(SecurityContext::getAuthentication) // 認証されたユーザーの結果 取得
				.map(Authentication::getName) // UserDetailsのgetName = 認証ユーザー識別子(userId)を取得し
				.map(UserId::of) // ユーザー識別子(userId)を独自の型にして
				.flatMap(this.repository::findById) // ユーザー識別子にて検索
				.map(this.userFactory::create) // 検索したユーザー情報(Entity)を User typeにして
				.orElse(null) // 返却(見つからない場合はnull)
		;
	}

	/**
	 * ユーザーの新規作成
	 * @param createUser 新規作成となるユーザー
	 * @return 新規登録ユーザー
	 */
	@Transactional
	public User create(CreateUserInput createUser) {
		// 前提条件
		if (exists(createUser)) throw new UserAlreadyExistsException(createUser.getEmail());// すでに存在するユーザーの場合

		// ユーザー 新規登録 を所定の方にして返却
		return this.userFactory.create(createUser(createUser));
	}
	/*********************************************
	 * 内部参照可能関数 (Domain Service)
	 *********************************************/
	/**
	 * 新規登録値の存在判定
	 * @param input 新規登録値
	 * @return true:新規登録ユーザー存在 / false:非存在
	 */
	private boolean exists(CreateUserInput input) {
		return repository.existsByEmail(input.getEmail());
	}

	/**
	 * ユーザーマスタ新規登録
	 * @param input 新規登録値
	 * @return 新規登録ユーザー値
	 */
	private MUser createUser(CreateUserInput input) {
		// 新規ユーザーIDの採番
		UserId userId = this.nextId();
		// 新規登録する 関連マスタ値の設定
		Set<UserRoleType> createRoleTypes = new HashSet<>(Arrays.asList(UserRoleType.create()));
		MUserProfile createProfile = toCreateUserProfile(userId);

		// 新規登録値から ユーザーマスタと 権限を新規登録
		return this.repository.saveAndFlush(
			MUser.builder()
				.userId(userId) // ユーザーID = 採番した値
				.nickname(input.getNickname())
				.email(input.getEmail())
				.password(this.passwordEncoder.encode(input.getPassword())) // パスワード = 入力値を暗号化したものでDB保存
				.roleTypes(createRoleTypes) // ユーザー権限
				.profile(createProfile) // ユーザープロフィール情報
				.createdAt(CreatedAt.create()) // 登録日 = 新規登録値 ドメイン
				.updatedAt(UpdatedAt.create()) // 更新日 = 新規登録値はドメイン
			.build()
		);
	}

	/**
	 * ユーザーのプロフィール新規登録値 へ
	 * @param userId 対象のユーザーID
	 * @return ユーザーのプロフィール新規登録値
	 */
	private MUserProfile toCreateUserProfile(UserId userId) {
		return MUserProfile
				.builder()
				.userId(userId) // ユーザーID継承
				// 氏名は初期値なし
				.createdAt(CreatedAt.create()) // 登録日 = 新規登録値 ドメイン
				.updatedAt(UpdatedAt.create()) // 更新日 = 新規登録値はドメイン
				.build();
	}
	/**
	 * @return ユーザーIDの採番
	 */
	private UserId nextId() { return UserId.of(this.repository.nextUserId()); }
	/*********************************************
	 * 外部参照可能関数 (JWT)
	 *********************************************/
	/**
	 * JWTの生成
	 * @param user 生成するユーザー record of m_user entity
	 * @return 生成するユーザーの JWT(文字列)
	 */
	public String createJWT(User user) {
		// 有効期限の生成
		Instant now = Instant.now();
		Instant expiry = Instant.now().plus(properties.getTokenExpiration());
		// JWTの生成
		return JWT
				.create()
				.withIssuer(properties.getTokenIssuer()) // 発行者
				.withIssuedAt(Date.from(now)) // 発行日
				.withExpiresAt(Date.from(expiry)) // 有効期限
				.withSubject(user.getId().toString()) // ユーザー識別子 = ログインID
				// TODO:その他付け加える情報あれば
				.sign(algorithm); // JWT暗号化
	}

	/**
	 * JWT の複合化
	 * @param token JWTの文字列
	 * @return JWT複合化した情報
	 */
	private Optional<DecodedJWT> decodedToken(String token) {
		// try and errorにて 情報解析
		try {
			// JWT の複合化
			return Optional.of(verifier.verify(token));
		} catch(JWTVerificationException ex) {
			// 複合化に失敗した場合は空 返却
			return Optional.empty();
		}
	}
}
