package vn.urban.vietnam.demologin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

/**
 * エントリーポイント
 */
@SpringBootApplication
public class DemoLoginApplication {
	/**
	 * SpringBoot アプリケーション起動
	 * @param args 起動の実行引数 (this SpringBoot Jar kick arguments.)
	 */
	public static void main(String[] args) {
		SpringApplication.run(DemoLoginApplication.class, args);
	}

	/**
	 * @return システム時間を日本時間に設定
	 */
	@Bean
	public Clock clock() {
		return Clock.system(ZoneId.of("Asia/Tokyo"));
	}
}
