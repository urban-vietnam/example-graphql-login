package vn.urban.vietnam.demologin.resolvers;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.demologin.service.ProfileService;
import vn.urban.vietnam.demologin.service.UserService;
import vn.urban.vietnam.demologin.types.Profile;
import vn.urban.vietnam.demologin.types.User;

import java.util.List;

/**
 * All Query Resolver
 */
@Component
@RequiredArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {
	/*********************************************
	 * filed
	 *********************************************/
	/** services */
	private final ProfileService profileService; // プロフィールサービス
	private final UserService userService; // 認証などのユーザーサービス

	/** TODO:動作確認環境よう */
	// TODO: パスワードの値を知りたいがためのやつ なので終わったら消す
	private final PasswordEncoder passwordEncoder;
	/*********************************************
	 * Queries
	 **********************************************/
	/**
	 * 指定ユーザーのプロフィール取得
	 * @param userId 指定ユーザーID
	 * @return 指定ユーザーのプロフィール
	 */
	@PreAuthorize("isAuthenticated()") // 認証されている人のみ 通信可能
	public Profile getProfile(final String userId) { return this.profileService.getProfile(userId); }

	/**
	 * sample for hasAuthority('ADMIN')
	 * @return ユーザー一覧 取得
	 */
	@PreAuthorize("hasAuthority('ADMIN')") // ADMIN権限のみ利用可能とする
	public List<Profile> getProfiles() { return  this.profileService.getProfiles(); }

	/**
	 * @return 現在のユーザー情報取得
	 */
	@PreAuthorize("isAuthenticated()")
	public User getCurrentUser() { return this.userService.getCurrentUser(); }
	/**
	 * Mutation createUserを作っていないので パスワードの暗号化の通信するテストのやつ
	 * @param password
	 * @return
	 */
	// TODO: パスワードの値を知りたいがためのやつ なので終わったら消す
	public String getPasswordEncode(final String password) { return passwordEncoder.encode(password); }
}
