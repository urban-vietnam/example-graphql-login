package vn.urban.vietnam.demologin.infrastructure.generator;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Properties;

/**
 * userIdなどのような接頭辞管理つきの文字列IDジェネレーター
 */
public class StringPrefixedSequenceIdGenerator extends SequenceStyleGenerator {
	/*********************************************
	 * 外部参照可能定数 (@GenericGenerator などの @Parameter関連)
	 *********************************************/
	/** 接頭辞の文字列値を利用する場合の 呼び出し方 */
	public static final String VALUE_PREFIX_PARAMETER = "valuePrefix";
	/** 接頭辞の文字列値を利用する場合の 接頭辞となる値のデフォルト値 */
	public static final String VALUE_PREFIX_DEFAULT = "";
	/** 数値フォーマットするID文字列を利用する場合のフォーマット設定の 呼び出し方 */
	public static final String NUMBER_FORMAT_PARAMETER = "numberFormat";
	/** 数値フォーマットするID文字列を利用する場合のフォーマット設定の デフォルト値 */
	public static final String NUMBER_FORMAT_DEFAULT = "%d";

	/*********************************************
	 * filed
	 *********************************************/
	// ID 接頭辞の文字列値
	private String valuePrefix;
	// ID 文字列 数字フォーマット値
	private String numberFormat;

	/**
	 * 接頭辞つきの文字列IDのNextId発行ルールの設定
	 * @param type
	 * @param params 呼び出しがわで 設定したいルールの内容
	 * @param serviceRegistry
	 * @throws MappingException
	 */
	@Override
	public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
		// 数値の持ち方としては BigDecimalTypeの数値までもて流ようにする
		super.configure(BigDecimalType.INSTANCE, params, serviceRegistry);
		// ルールの内容から独自 接頭辞文字列ID発行 処理するために 保存
		this.valuePrefix = ConfigurationHelper.getString(VALUE_PREFIX_PARAMETER, params, VALUE_PREFIX_DEFAULT);
		this.numberFormat = ConfigurationHelper.getString(NUMBER_FORMAT_PARAMETER, params, NUMBER_FORMAT_DEFAULT);
	}

	/**
	 * ルールに基づいた Next IDの生成
	 * @param session
	 * @param object
	 * @return ルールに基づいた Next ID (接頭辞付きの 数字ID文字列)
	 * @throws HibernateException
	 */
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		// Next ID = 接頭辞 + 数字フォーマット(発行した次のシーケンス値)を組み合わせた文字列
		return valuePrefix + String.format(numberFormat, super.generate(session, object));
	}
}
