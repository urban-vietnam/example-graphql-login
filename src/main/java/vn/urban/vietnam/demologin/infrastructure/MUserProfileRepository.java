package vn.urban.vietnam.demologin.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.urban.vietnam.demologin.infrastructure.entity.MUserProfile;
import vn.urban.vietnam.demologin.model.value.UserId;

/**
 * m_user_profiles repository
 */
public interface MUserProfileRepository extends JpaRepository<MUserProfile, UserId> {
}
