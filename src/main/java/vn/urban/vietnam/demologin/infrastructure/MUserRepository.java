package vn.urban.vietnam.demologin.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.urban.vietnam.demologin.infrastructure.entity.MUser;
import vn.urban.vietnam.demologin.model.value.UserId;

import java.util.Optional;

/**
 * m_user repository
 */
public interface MUserRepository extends JpaRepository<MUser, UserId> {
	/**
	 * Email から 該当のユーザー検索
	 * @param email Email
	 * @return 該当ユーザー
	 */
	public Optional<MUser> findByEmail(String email);

	/**
	 * 指定Email のユーザ存在確認 (重複確認用)
	 * @param email Email
	 * @return true:exist / false: not exist
	 */
	public boolean existsByEmail(String email);

	/**
	 * @return ユーザーIDのふり番
	 */
	@Query(value = "SELECT nextval('user_id_sequence')", nativeQuery = true)
	public Long nextUserId();
}
