package vn.urban.vietnam.demologin.model.value;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.IdentifierGenerator;
import vn.urban.vietnam.demologin.infrastructure.generator.StringPrefixedSequenceIdGenerator;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;

/**
 * domain value object
 */
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor(staticName = "private")
public class UserId implements Serializable {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	private static final String FORMAT = "%010d";
	/*********************************************
	 * Filed
	 *********************************************/
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_sequence") // user_id_sequenceを利用し発行
	@GenericGenerator( // 数値文字列IDとして FORMAT に準拠したID生成をさせる(Domain)
			name = "user_id_sequence",
			strategy = "vn.urban.vietnam.demologin.infrastructure.generator.StringPrefixedSequenceIdGenerator",
			parameters = {
				@Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = FORMAT)
			}
	)
	@Column(name = "id", updatable = false)
	private String value;
	/*********************************************
	 * 外部参照可能関数 (static)
	 *********************************************/
	public static UserId of(String id) { return new UserId(id); }
	/**
	 * 数値から変換
	 * @param id userIdになる数値
	 * @return UserId
	 */
	public static UserId of(int id) { return new UserId(String.format(FORMAT, id)); }
	public static UserId of(long id) { return new UserId(String.format(FORMAT, id)); }
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	@Override
	public String toString() { return value; }
}
