package vn.urban.vietnam.demologin.model;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.demologin.infrastructure.entity.MUser;
import vn.urban.vietnam.demologin.infrastructure.entity.MUserProfile;
import vn.urban.vietnam.demologin.types.Profile;
import vn.urban.vietnam.demologin.types.User;

import java.util.Objects;
import java.util.Optional;

/**
 * type of User factory class
 */
@Component
@RequiredArgsConstructor
public class UserFactory {
	/*********************************************
	 * filed
	 *********************************************/
	/** 型 生成器 */
	private final ProfileFactory profileFactory;

	/**
	 * create User for MUser Tables
	 * @param entity record of m_users
	 * @return User result.
	 */
	public User create(MUser entity) {
		// 前提条件
		if (Objects.isNull(entity)) return new User();

		// MUser Tables entity -> Profile (Nullable)
		Profile profile = this.profileFactory.create(entity);
		// MUser Tables entity -> User
		return User.builder()
				// set basic user info
				.id(entity.getUserId())
				.roleTypes(entity.getRoleTypes())
				// set profile
				.profile(profile)
				.build()
		;
	}
}
