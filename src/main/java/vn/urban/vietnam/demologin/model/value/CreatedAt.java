package vn.urban.vietnam.demologin.model.value;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * domain value object
 */
@Embeddable
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreatedAt implements Serializable {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	/*********************************************
	 * Filed
	 *********************************************/
	/** 値 (not change value = クラスを生成した時の yyyy-MM-dd hh:mm:ss形式の文字列) */
	@Column(name = "created_at", updatable = false)
	private Date value;

	/*********************************************
	 * 外部参照可能関数 (static)
	 *********************************************/
	/**
	 * 指定値 -> CreatedAt
	 * @param value 指定値
	 * @return CreatedAt value object
	 */
	public static CreatedAt of(Date value) { return new CreatedAt(value); }
	/**
	 * @return 新規登録値
	 */
	public static CreatedAt create() { return of(new Date()); }
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	@Override
	public String toString() { return DATE_FORMAT.format(value); } // yyyy-MM-dd hh:mm:ss
}
