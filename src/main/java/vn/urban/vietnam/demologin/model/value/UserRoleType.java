package vn.urban.vietnam.demologin.model.value;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

/**
 * ユーザー権限 設定値 (USER, ADMIN ... etcなどの)
 * domain value object
 */
@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleType implements Serializable {
	/*********************************************
	 * Domain 定数
	 *********************************************/
	/** デフォルト 権限の値 */
	private static final String DEFAULT_USER_ROLE_TYPE = "USER";
	/** システム管理者 権限の値 */
	private static final String ADMIN_USER_ROLE_TYPE = "ADMIN";
	/** 権限の値 */
	public static final String[] USER_ROLE_TYPES = {DEFAULT_USER_ROLE_TYPE, ADMIN_USER_ROLE_TYPE};
	/*********************************************
	 * Filed
	 *********************************************/
	/** 値 (change able value) */
	@Column(name = "role_type") // m_user_roleでの権限の値の項目名
	private String value;

	/*********************************************
	 * 外部参照可能関数 (static)
	 *********************************************/
	/**
	 * 指定値 -> UserRoleType
	 * @param value 指定値
	 * @return UserRoleType value object
	 */
	public static UserRoleType of(String value) { return new UserRoleType(value); }
	/**
	 * @return 新規登録値
	 */
	public static UserRoleType create() { return of(DEFAULT_USER_ROLE_TYPE); }
	/**
	 * @return システム管理者権限値
	 */
	public static UserRoleType createAdmin() { return of(ADMIN_USER_ROLE_TYPE); }
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	@Override
	public String toString() { return value; }
}
